let Sequelize = require('sequelize');

let { DB_NAME, DB_USER, DB_PASSWORD, DB_HOST } = process.env;

module.exports = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
  dialect: 'mysql',
  host: DB_HOST
});
