let Twitter = require('twitter');
let moment = require('moment');

function normalizeTweets(apiResponse) {
  return apiResponse.statuses.map((tweet) => {
    return {
      id: tweet.id_str,
      text: tweet.text,
      // createdAt: moment(tweet.created_at),
      createdAt: tweet.created_at,
      username: tweet.user.screen_name
    };
  });
}

let client = new Twitter({
  consumer_key: process.env.consumer_key,
  consumer_secret: process.env.consumer_secret,
  access_token_key: process.env.access_token_key,
  access_token_secret: process.env.access_token_secret
});

function getTweets(hashTag, sinceID) {
  return new Promise(function(resolve, reject) {
    let params = {
      q: `#${hashTag}`,
      count: 100
    };

    console.log('since_id', sinceID);
    if (sinceID || sinceID !== 0) {
      params.since_id = sinceID;
    }

    console.log(params);
    client.get('search/tweets', params, function(error, apiResponse) {
      if (error) {
        console.log(error);
        reject(error);
      } else {
        // resolve(apiResponse);
        console.log('statuses.length', apiResponse.statuses.length);
        resolve({
          hasTweets: apiResponse.search_metadata.since_id > 0,
          tweets: normalizeTweets(apiResponse),
          sinceID: apiResponse.search_metadata.since_id
        });
      }
    });
  });
}

function recursivelyFetch(hashTag, data) {
  if (data.count < 5) {
    console.log('called', data.count);
    data.count += 1;
    if (data.hasTweets) {
      return getTweets(hashTag, data.sinceID).then((newData) => {
        // console.log(newData.tweets.length);
        newData.tweets = newData.tweets.concat(data.tweets);
        newData.count = data.count;
        return recursivelyFetch(hashTag, newData);
      });
    } else {
      return data.tweets;
    }
  } else {
    return data.tweets;
  }
}

module.exports = {
  getTweetsWithHashTag(hashTag) {
    // return getTweets(hashTag);
    return recursivelyFetch(hashTag, {
      hasTweets: 1000,
      tweets: [],
      count: 0
    });
  }
};
