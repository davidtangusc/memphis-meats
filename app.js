require('dotenv').config();
let app = require('koa')();
let router = require('koa-router')();
let twitterAPI = require('./twitter-api');
let Tweet = require('./models/tweet');

router.get('/tweets', function *() {
  let tweets = yield twitterAPI.getTweetsWithHashTag('mmfirstbite');
  for (tweetData of tweets) {
    try {
      let tweet = Tweet.build(tweetData);
      yield tweet.save()
    } catch(e) {
      console.log(`Skipping tweet ${tweetData.id}`);
    }
  }

  let storedTweets = yield Tweet.findAll();
  this.body = storedTweets.map((tweet) => {
    return {
      username: tweet.username,
      text: tweet.text,
      link: `https://twitter.com/${tweet.username}/status/${tweet.id}`
    };
  });
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(process.env.PORT || 3000);
