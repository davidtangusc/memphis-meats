let Sequelize = require('sequelize');
let sequelize = require('./../config/database');

module.exports = sequelize.define('tweet', {
  username: {
    type: Sequelize.STRING
  },
  text: {
    type: Sequelize.STRING
  }
  // tweetID: {
  //   type: Sequelize.STRING,
  //   field: 'tweet_id',
  //   unique: true
  // }
}, {
  timestamps: false
});
